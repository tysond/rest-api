# REST API

## what is this?
simple rest api example using flask. a flask api object contains one or more functionalities (GET, POST, etc). 


## install


pip install -r requirements.txt

##steps


My code walk-through is as follows

*  I created  Salary dataset and saved it in csv form

*  Dumped that CSV  into my SQLite db.

*  Used SQLAlchemy to connect to database and do select operations.

*  Created Flask-Restful classes to map functions with API URL

*  Returned the queried data as JSON ,which can be used universally.


## this is how it works
$ sqlite3 salaries.db
sqlite> .mode csv salaries
sqlite> .import employee.csv salaries
##libraries
$ pip install flask
$ pip install flask-restful
$ pip install sqlalchemy
## run

python app.py


then go to http://localhost:5000/departments

you could drill down by deparments too!

try http://localhost:5000/dept/police

try http://localhost:5000/dept/fire
etc etc
